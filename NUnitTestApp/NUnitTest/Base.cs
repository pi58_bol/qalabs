﻿using NUnit.Framework;
using NUnit.Framework.Api;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTest
{
    class Base
    {
        public TestData testData = new TestData();
        public Locators locators = new Locators();
        public Helper helper = new Helper();
        public IWebDriver driver = new ChromeDriver(@"D:\Education\QA\Lab3\NUnitTestApp\NUnitTest\driver\");
        [SetUp]
        public void Setup()
        {
            driver.Url = "http://facebook.com";
            helper.driver = driver;
            /*options.AddArguments("--disable-extensions"); // to disable extension
            options.AddArguments("--disable-notifications"); // to disable notification
            options.AddArguments("--disable-application-cache"); // to disable cache*/
        }
        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                string path = @"D:\Education\QA\Lab3\NUnitTestApp\NUnitTest\results";
                var imagename = $"results_{DateTime.Now:yyyy-MM-dd_HH-mm-ss.fffff}.png";
                var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(path + imagename);
            }

            //driver.Quit();
        }
    }
}
