﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTest
{
    class Helper
    {
        TestData testData = new TestData();
        public IWebDriver driver;
        public string TakeScreenshot()
        {
            string path = testData.screenshotPath;
            var imagename = $"results_{DateTime.Now:yyyy-MM-dd_HH-mm-ss.fffff}";
            var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile(path + imagename + ".png");
            return imagename;
        }
    }
}
