﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;

namespace NUnitTest
{
    class Locators
    {
        public string main_url = "https://www.facebook.com/";
        public By loginScreenEmailField = By.Id("email");
        public By loginScreenPassField = By.Name("pass");
        public By loginScreenEnterBtn = By.Id("u_0_b");
        //
        public By postFiled = By.XPath("//span[text()='Что у вас нового, Алексей?']");//By.XPath("/html/body/div[1]/div/div[1]/div/div[3]/div/div/div[1]/div[1]/div/div[2]/div/div/div[3]/div/div[2]/div/div/div/div[1]/div/div[1]/span");//By.XPath("//span[text()='Что у вас нового, Алексей?']");
        public By postInputField = By.XPath("//*[@id='mount_0_0']/div/div[1]/div/div[4]/div/div/div[1]/div/div[2]/div/div/div/form/div/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/div/div[2]/div/div/div/div");
        public By postChangeVisibilityBtn = By.XPath("//*[@id='mount_0_0']/div/div[1]/div/div[4]/div/div/div[1]/div/div[2]/div/div/div/form/div/div[1]/div/div/div[1]/div[2]/div[2]/div/div/div/div/div/span/div/div");
        public By postVisibilityTypePrivate = By.XPath("/html/body/div[1]/div/div[1]/div/div[4]/div/div/div[1]/div/div[2]/div/div/div/form/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/div/div[5]/div");
        public By potBtn = By.XPath("//*[@id='mount_0_0']/div/div[1]/div/div[4]/div/div/div[1]/div/div[2]/div/div/div/form/div/div[1]/div/div/div[3]/div[2]/div");
        public By likeBtn = By.XPath("//div[@aria-label='Нравится']");
        public By postActions = By.XPath("//*[@id='mount_0_0']/div/div[1]/div/div[3]/div/div/div[1]/div[1]/div/div/div[4]/div[2]/div/div[2]/div[3]/div[1]/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div[3]");
        public By postRemoveBtn = By.XPath("//*[@id='mount_0_0']/div/div[1]/div/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div[1]/div/div[1]/div/div[8]/div[2]/div/div[1]/span");
        public By postRemoveConfirmBtn = By.XPath("//*[@id='mount_0_0']/div/div[1]/div/div[4]/div/div/div[1]/div/div[2]/div/div/div/div[4]/div/div[1]/div[1]/div[1]/div/span");
        //Search
        public By searchInput = By.XPath("//input[@type = 'search']");
        public By searchSubmit = By.XPath("/html/body/div[1]/div/div[1]/div/div[2]/div[2]/div/div[2]/div/ul/li[2]/div/a/div/div[2]/span");
        //AddFriend
        public By firstSearchResult = By.XPath("/html/body/div[1]/div/div[1]/div/div[3]/div/div/div[1]/div[1]/div[2]/div/div/div/div/div/div/div[1]/div/div/div/div/div/div[1]/div[2]/div[1]/div/div/div[2]/div/div[1]/h3/span/span/div/a/span");
        //Message
        public By messBtn = By.XPath("/html/body/div[1]/div/div[1]/div/div[5]/div/div[1]/div[2]/span");
        public By messSearchInput = By.XPath("/html/body/div[1]/div/div[1]/div/div[5]/div/div[1]/div[1]/div[1]/div/div/div/div/div/div[2]/div/div[2]/div[1]/div/div/input");
        public By messLiFirst = By.XPath("/html/body/div[1]/div/div[1]/div/div[5]/div/div[1]/div[1]/div[1]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div/div[1]/ul/div[1]/li[1]/div/a/div/div[2]/div/div/span");
        public By messInput = By.XPath("/html/body/div[1]/div/div[1]/div/div[5]/div/div[1]/div[1]/div[1]/div/div/div/div/div/div[3]/div/div/div[2]/form/div/div[3]/div[2]/div[1]/div/div/div/div/div[2]/div/div/div/div/span");
        public By messSendBtn = By.XPath("/html/body/div[1]/div/div[1]/div/div[5]/div/div[1]/div[1]/div[1]/div/div/div/div/div/div[3]/div/div/div[2]/form/div/div[3]/span[2]");
        public By messClose = By.XPath("/html/body/div[1]/div/div[1]/div/div[5]/div/div[1]/div[1]/div[1]/div/div/div/div/div/div/div[1]/div/div/div[3]/span[2]");
        //Stories
        public By storieFirst = By.XPath("/html/body/div[1]/div/div[1]/div/div[3]/div/div/div[1]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div[1]/div/div/div/a");
        public By storieNextBtn = By.XPath("/html/body/div[1]/div/div[1]/div/div[4]/div/div/div[1]/div/div[3]/div[2]/div/div/div/div/div[3]/div/div/div/div[1]/div[1]/div[3]/div");
        //Watch
        public By GetWatchLikeBtnPath(int id)
        {
            string xPath = "//*[@id='watch_feed']/div/div/div/div/div/div["+ id + "]/div/div/div/div/div[2]/div[3]/div/div/div[1]/div/span[1]/div/div/span/div[1]";
            return By.XPath(xPath);
        }
        //Bio
        public By bioBtn = By.XPath("/html/body/div[1]/div/div[1]/div/div[3]/div/div/div[1]/div[1]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div/span/span/div");
        public By bioInput = By.XPath("/html/body/div[1]/div/div[1]/div/div[3]/div/div/div[1]/div[1]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div/span/div/div/label/textarea");
        public By bioSubmit = By.XPath("/html/body/div[1]/div/div[1]/div/div[3]/div/div/div[1]/div[1]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div/span/div/div/div[2]/div[2]/div[2]");
        public By bioSubmitNow = By.XPath("/html/body/div[1]/div/div[1]/div/div[3]/div/div/div[1]/div[1]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div/span/div/div/div[2]/div[2]/div[2]");
        //Logout
        public By profBtn = By.XPath("/html/body/div[1]/div/div[1]/div/div[2]/div[4]/div[1]/span/div/div[1]");
        public By logoutBtn = By.XPath("//*[@id='mount_0_0']/div/div[1]/div/div[2]/div[4]/div[2]/div/div[2]/div[1]/div[1]/div/div/div/div/div/div/div/div/div[1]/div/div[3]/div/div[4]/div");
    }
}
