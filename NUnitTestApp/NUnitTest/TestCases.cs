﻿using NUnit.Framework;
using NUnit.Framework.Api;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Interactions;
using NUnit.Allure;
using Allure.Commons;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;

namespace NUnitTest
{
    [TestFixture]
    [AllureNUnit]
    class TestCases : Base
    {
        [Test, Order(1)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void LoginTest()
        {
            Thread.Sleep(1000);
            IWebElement ele = driver.FindElement(locators.loginScreenEmailField);
            //enter the username value  
            ele.SendKeys(testData.username);
            Thread.Sleep(1000);
            AllureLifecycle.Instance.AddAttachment("Screen", null , testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Console.Write("username value is entered \n");
            //identify the password text box  
            IWebElement ele1 = driver.FindElement(locators.loginScreenPassField);
            //enter the password value  
            ele1.SendKeys(testData.password);//
            Console.Write("password is entered \n");
            //click on the Login button  
            IWebElement ele2 = driver.FindElement(locators.loginScreenEnterBtn);
            ele2.Click();
            Thread.Sleep(3000);
            Console.Write("login button is clicked \n");
        }
        [Test, Order(2)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void PostTest()
        {
            Thread.Sleep(4000);
            IWebElement post_field = driver.FindElement(locators.postFiled);
            post_field.Click();
            Thread.Sleep(1000);
            driver.FindElement(locators.postChangeVisibilityBtn).Click();
            Thread.Sleep(1000);
            driver.FindElement(locators.postVisibilityTypePrivate).Click();
            Thread.Sleep(2000);
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            IWebElement post_input_field = driver.FindElement(locators.postInputField);
            post_input_field.Click();
            Thread.Sleep(1000);
            post_input_field.SendKeys(testData.postText);
            Thread.Sleep(1000);
            driver.FindElement(locators.potBtn).Click();
            Thread.Sleep(1000);
        }
        [Test, Order(3)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void LikeClickTest()
        {
            Thread.Sleep(3000);
            IWebElement likeBtn = driver.FindElement(locators.likeBtn);
            Actions actions = new Actions(driver);
            actions.MoveToElement(likeBtn);
            actions.Perform();
            Thread.Sleep(2000);
            likeBtn.Click();
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Thread.Sleep(2000);
        }
        [Test, Order(4)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void RemovePostTest()
        {
            Thread.Sleep(3000);
            driver.Navigate().GoToUrl(testData.profileLink);
            Thread.Sleep(3000);
            IWebElement postActionsBtn = driver.FindElement(locators.postActions);
            Actions actions = new Actions(driver);
            Thread.Sleep(2000);
            postActionsBtn.Click();
            Thread.Sleep(2000);
            IWebElement postActionsRemoveBtn = driver.FindElement(locators.postRemoveBtn);
            postActionsRemoveBtn.Click();
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Thread.Sleep(2000);
            IWebElement postRemoveConfirmBtn = driver.FindElement(locators.postRemoveConfirmBtn);
            postRemoveConfirmBtn.Click();
            Thread.Sleep(2000);
        }
        [Test, Order(5)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void SearchTest()
        {
            Thread.Sleep(3000);
            IWebElement searchInput = driver.FindElement(locators.searchInput);
            searchInput.Click();
            searchInput.SendKeys(Faker.Name.FirstName());
            Thread.Sleep(2000);
            driver.FindElement(locators.searchSubmit).Click();
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Thread.Sleep(2000);
        }
        [Test, Order(6)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void MessageTest()
        {
            Thread.Sleep(3000);
            driver.Navigate().GoToUrl(driver.Url);
            Thread.Sleep(8000);
            IWebElement messBtn = driver.FindElement(locators.messBtn);
            messBtn.Click();
            Thread.Sleep(1000);
            IWebElement searchInput = driver.FindElement(locators.messSearchInput);
            searchInput.SendKeys(testData.messAdresat);
            Thread.Sleep(2000);
            driver.FindElement(locators.messLiFirst).Click();
            Thread.Sleep(2000);
            IWebElement messInput = driver.FindElement(locators.messInput);
            messInput.SendKeys(testData.messTestMessage);
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Thread.Sleep(1000);
            messInput.SendKeys(Keys.Return);
            Thread.Sleep(1000);
            driver.FindElement(locators.messClose).Click();
        }
        [Test, Order(7)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void StoriesTest()
        {
            Thread.Sleep(1000);
            IWebElement storieBtn = driver.FindElement(locators.storieFirst);
            storieBtn.Click();
            for (int i = 0; i < 2; i++)
            {
                Thread.Sleep(500);
                IWebElement searchInput = driver.FindElement(locators.storieNextBtn);
                searchInput.Click();
            }
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Thread.Sleep(2000);
        }
        [Test, Order(8)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void WatchTest()
        {
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl(testData.watchLink);
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Thread.Sleep(1000);
            for (int i = 1; i < 5; i++)
            {
                Thread.Sleep(2000);
                IWebElement searchInput = driver.FindElement(locators.GetWatchLikeBtnPath(i));
                Actions actions = new Actions(driver);
                actions.MoveToElement(searchInput);
                actions.Perform();
                Thread.Sleep(1000);
                searchInput.Click();
            }
            Thread.Sleep(2000);
        }
        [Test, Order(9)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void BioTest()
        {
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl(testData.profileLink);
            Thread.Sleep(1000);
            IWebElement bioBtn = driver.FindElement(locators.bioBtn);
            bioBtn.Click();
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Thread.Sleep(1000);
            IWebElement searchInput = driver.FindElement(locators.bioInput);
            searchInput.SendKeys(testData.bioTextMessage);
            Thread.Sleep(1000);
            driver.FindElement(locators.bioSubmit).Click();
            Thread.Sleep(2000);
            driver.FindElement(locators.bioSubmitNow).Click();
            Thread.Sleep(2000);
        }
        [Test, Order(10)]
        [AllureStory]
        [AllureTag("NUnit")]
        public void LogoutTest()
        {
            Thread.Sleep(1000);
            IWebElement profBtn = driver.FindElement(locators.profBtn);
            profBtn.Click();
            AllureLifecycle.Instance.AddAttachment("Screen", null, testData.screenshotPath + helper.TakeScreenshot() + ".png");
            Thread.Sleep(1000);
            IWebElement logoutBtn = driver.FindElement(locators.logoutBtn);
            logoutBtn.Click();
            Thread.Sleep(4000);
            driver.Quit();
        }
    }
}
