﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTest
{
    class TestData
    {
        //System
        public string screenshotPath = @"NUnitTest\results\";
        //Tests
        public string username = "bovalex@ukr.net";
        public string password = "password";
        public string messAdresat = "Алексей";
        public string messTestMessage = "This is an autotest message, LMAO!";
        public string bioTextMessage = "I LOVE QA";
        public string profileLink = "https://www.facebook.com/bovalex/";
        public string watchLink = "https://www.facebook.com/watch";
        public string postText = "Autotest Post. Don't mind.";
    }
}
